import React, {Component} from 'react';
import {Card, CardBody, CardHeader} from "reactstrap";
import '../css/login.css';
import {Redirect} from "react-router-dom";


class Login extends Component {


    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            correctUsername: 'illia',
            correctPassword: 'illia3',
            incorrectUsername: false,
            incorrectPassword: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeAlert = this.removeAlert.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const name = target.id;
        this.removeAlert(name);
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    removeAlert(name) {
        if (name === 'username') {
            this.setState({
                incorrectUsername: false
            });
        } else if (name === 'password') {
            this.setState({
                incorrectPassword: false
            });
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.username !== this.state.correctUsername) {
            this.setState({
                incorrectUsername: true
            });
        } else if (this.state.password !== this.state.correctPassword) {
            this.setState({
                incorrectPassword: true
            });
        }
        else if (this.state.username === this.state.correctUsername && this.state.password === this.state.correctPassword) {
            this.props.log(true);
        }

    }



    render() {
        return (
            <>
                {this.props.userLoggedIn ? <Redirect to='/weather'/> : null}
                <div className='login-page'>
                    <img draggable={false} className='img-background' src='https://images.saatchiart.com/saatchi/424173/art/5339263/4409077-HSC00001-7.jpg' alt='some photo'/>
                </div>

                <div className='container login-style'>
                    <div className='row justify-content-center align-items-center'>
                        <div className='mt-5 col-md-5 col-8 ml-auto mr-auto'>
                            <Card>
                                <CardHeader><h2 align='center'>Log in</h2></CardHeader>
                                <CardBody>
                                    <form onSubmit={this.handleSubmit} className='container'>
                                        <div className='row align-items-center form-group'>
                                            <label htmlFor='username' className='col-12 col-lg-4'>Username</label>
                                            <input id='username' onChange={(event) => this.handleInputChange(event)} className='form-control col-lg-6 col-12' type='text' required={true} placeholder='Bill'/>
                                            {this.state.incorrectUsername && <div className='col-12 mt-1 alert alert-danger'>
                                                <strong>Log in failed!</strong> Incorrect username!
                                            </div>}
                                        </div>
                                        <div className='row mt-5 mt-lg-0 align-items-center form-group'>
                                            <label htmlFor='password' className='col-12 col-lg-4'>Password</label>
                                            <input id='password' onChange={(event) => this.handleInputChange(event)} className='form-control col-lg-6 col-12' type='password' required={true}/>
                                            {this.state.incorrectPassword && <div className='col-12 mt-1 alert alert-danger'>
                                                <strong>Log in failed!</strong> Incorrect password!
                                            </div>}
                                        </div>
                                        <hr/>

                                        <button type='submit' className='mt-1 btn-lg btn btn-primary'>Log in</button>

                                    </form>
                                </CardBody>
                            </Card>
                        </div>

                    </div>

                </div>
            </>

        );
    }
}


export default Login;






// import React, {Component} from 'react';
// import {Card, CardBody, CardHeader} from "reactstrap";
// import '../css/login.css';
// import {Redirect} from "react-router-dom";
//
//
// class Login extends Component {
//
//
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             logIn: true,
//             username: '',
//             password: '',
//             success: false
//         };
//
//         this.changeAction = this.changeAction.bind(this);
//         this.handleInputChange = this.handleInputChange.bind(this);
//         this.handleSubmit = this.handleSubmit.bind(this);
//     }
//
//     changeAction() {
//        console.log('Here');
//         this.setState({
//             logIn: !this.state.logIn
//         });
//     }
//
//     handleInputChange(event) {
//         const target = event.target;
//         const name = target.id;
//         console.log(name);
//         const value = target.value;
//         this.setState({
//             [name]: value
//         });
//     }
//
//     handleSubmit(event) {
//         event.preventDefault();
//         if(this.state.logIn) {
//             // check user info
//         } else {
//             // add new user
//         }
//         this.setState({
//             success: this.state.username === 'user1'
//         });
//     }
//
//
//
//     render() {
//         return (
//             <>
//                 {this.state.success ? <Redirect to='/weather'/> : null}
//                 <div className='login-page'>
//                     <img draggable={false} className='img-background' src='https://images.saatchiart.com/saatchi/424173/art/5339263/4409077-HSC00001-7.jpg' alt='some photo'/>
//                 </div>
//
//                 <div className='container login-style'>
//                     <div className='row justify-content-center align-items-center'>
//                         <div className='mt-5 col-md-5 col-8 ml-auto mr-auto'>
//                             <Card>
//                                 <CardHeader><h2 align='center'>{this.state.logIn ? 'Log in': 'Sign in'}</h2></CardHeader>
//                                 <CardBody>
//                                     <form onSubmit={this.handleSubmit} className='container'>
//                                         <div className='row align-items-center form-group'>
//                                             <label htmlFor='username' className='col-12 col-lg-4'>Username</label>
//                                             <input id='username' onChange={(event) => this.handleInputChange(event)} className='form-control col-lg-6 col-12' type='text' required={true} placeholder='Bill'/>
//                                         </div>
//                                         <div className='row mt-3 mt-lg-0 align-items-center form-group'>
//                                             <label htmlFor='password' className='col-12 col-lg-4'>Password</label>
//                                             <input id='password' onChange={(event) => this.handleInputChange(event)} className='form-control col-lg-6 col-12' type='password' required={true}/>
//                                         </div>
//                                         <div className='row row mt-3 mt-lg-0 mb-4 align-items-center form-group'>
//                                             <label htmlFor='newUser' className='col-lg-4 col-6'>New User?</label>
//                                             <input onChange={this.changeAction} id='newUser' className='form-check' type='checkbox'/>
//                                         </div>
//                                         <hr/>
//
//                                         <button type='submit' className='mt-3 btn-lg btn btn-primary'>{this.state.logIn ? 'Log in': 'Sign in'}</button>
//
//                                     </form>
//                                 </CardBody>
//                             </Card>
//                         </div>
//
//                     </div>
//
//                 </div>
//             </>
//
//         );
//     }
// }
//
//
// export default Login;
//
