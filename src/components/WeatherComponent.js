import React, {Component} from "react";
import '../css/weather.css';
import {Redirect} from "react-router-dom";
import BlankPage from "./BlankPage";
import {Button, Card, CardBody, CardHeader, CardImg, CardText, Navbar, NavbarBrand} from "reactstrap";
import * as dayjs from "dayjs";

const basicUrl = 'https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/';

function RenderDay({day}) {

    const date = dayjs(day.applicable_date);
    const today = dayjs(new Date());
    const tomorrow = today.add(1,'day');
    let dayName = date.format('ddd D MMM');
    if (date.isSame(today,'day')) {
        dayName = 'Today';
    } else if (date.isSame(tomorrow,'day')) {
        dayName = 'Tomorrow';
    }

    return (
        <Card className='day-card'>
            <CardHeader>{dayName}</CardHeader>
            <CardBody className='day-card-body'>
                <CardImg object src={`https://www.metaweather.com/static/img/weather/${day.weather_state_abbr}.svg`} alt={`${day.weather_state_name}`}/>
                <CardText>
                    <div className='mt-2 day-description'>
                        <span>{day.weather_state_name}<br/></span>
                        <span>Max:</span> {Number.parseFloat(day.max_temp).toFixed(2)}°C<br/>
                        <span>Min:</span> {Number.parseFloat(day.min_temp).toFixed(2)}°C<br/>
                        <span>Humidity:</span> {day.humidity}%<br/>
                        <span>Visibility:</span> {Number.parseFloat(day.visibility).toFixed(1)} miles<br/>
                        <span>Pressure:</span> {Math.round(Number.parseFloat(day.air_pressure))} mb <br/>
                        <span>Confidence:</span> {day.predictability}%<br/>
                    </div>
                </CardText>
            </CardBody>
        </Card>
    );
}

class Weather extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cityName: 'Kyiv',
            latitude: 50.441380,
            longitude: 30.522490,
            woeid: 924938,
            weather: [],
            weatherIsLoading: true,
            useUserLocation: false
        }

        this.fetchCityByLatLong = this.fetchCityByLatLong.bind(this);
        this.fetchForecast = this.fetchForecast.bind(this);
        this.getGeoLocation = this.getGeoLocation.bind(this);
        this.fetchCityWeather = this.fetchCityWeather.bind(this);
    }

    getGeoLocation() {

        // const options = {
        //     enableHighAccuracy: true,
        //     timeout: 5,
        //     maximumAge: 0
        // };

        console.log('getting location...');
        const geo = navigator.geolocation;
        geo.getCurrentPosition((position) => {
            this.setState({
                useUserLocation: true,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
            console.log('got location!');
            console.log(position.coords.latitude + ' '+ position.coords.longitude);
            this.fetchForecast();
        },() => {
            console.log('User location denied');
            this.fetchForecast();
        });
    }

    fetchForecast() {
        console.log('fetching forecast...');

        if (this.state.useUserLocation) {
            console.log('using user location...');
            const url = basicUrl + `search/?lattlong=${this.state.latitude},${this.state.longitude}`;
            this.fetchCityByLatLong(url)
                .then(this.fetchCityWeather);
        } else {
            console.log('using default location...');
            this.fetchCityWeather();
        }
    }

    fetchCityWeather() {
        console.log('getting city weather...');
        const url = basicUrl + `${this.state.woeid}`;
        fetch(url)
            .then((res) => res.json())
            .then((json) => {

                const weather = json.consolidated_weather;

                this.setState({
                    weatherIsLoading: false,
                    weather: weather
                });
                console.log('got city weather!');
                console.log(weather);
            })
            .catch((err) => {
                console.log(err);
            });
    }


    fetchCityByLatLong(url) {
        return fetch(url)
            .then((res) => res.json())
            .then((cities) => {
                const city = cities[0];
                const latlong = city.latt_long.split(',').map((n) => Number.parseFloat(n));

                this.setState({
                    woeid: city.woeid,
                    cityName: city.title,
                    latitude: latlong[0],
                    longitude: latlong[1]
                });

                console.log('got cities!');
                console.log(cities);
            });
    }

    componentDidMount() {
        this.getGeoLocation();
    }


    render() {

        const days = this.state.weather.map((d) => {
            return (
            <div key={d.id} className='col-lg-2 col-4 mb-4'>
                <RenderDay day={d}/>
            </div>
            );
        });

        return (
            <div>
                {!this.props.userLoggedIn ? <Redirect to='/login'/> : null}
                <Navbar className='navbar-light header'>
                    <NavbarBrand className='navbrand-style' href='/weather'>Weather App</NavbarBrand>
                    <Button className='btn-logout' onClick={() => this.props.log(false)}>Log out</Button>
                </Navbar>
                <div className='mt-5 container'>
                    <h1>{this.state.cityName === '' ? 'Getting your city...':this.state.cityName}</h1>
                    <hr/>
                    <BlankPage condition={this.state.weatherIsLoading} loadingSpinner={true} message={'Loading forecast for your city...'}/>
                    <div className='row mt-4'>
                        {days}
                    </div>
                </div>
            </div>
        );
    }
}



export default Weather;










// fetchForecast() {
//     if (this.state.useUserLocation) {
//         fetch('https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/922137/')
//             .then((res) => {
//                 console.log(res);
//             })
//             .catch((err) => {
//                 console.log(err);
//             });
//     }
// }

