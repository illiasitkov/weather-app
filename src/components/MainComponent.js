import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./LoginComponent";
import Weather from "./WeatherComponent";




class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userLoggedIn: false
        };

        this.log = this.log.bind(this);
    }



    log(logIn) {
        this.setState({
            userLoggedIn: logIn
        });
    }





    render() {
        return (
            <div>
                <Switch>
                    <Route exact path='/weather'>
                        <Weather log={this.log} userLoggedIn={this.state.userLoggedIn}/>
                    </Route>
                    <Route exact path='/login'>
                        <Login userLoggedIn={this.state.userLoggedIn} log={this.log}/>
                    </Route>
                    <Redirect to='/login'/>
                </Switch>
            </div>
        );
    }

}



export default Main;












